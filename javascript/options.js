function formatLastModTime(lastmod0) {
    var lastmod = parseInt(lastmod0,10);
    if (lastmod > 0) {
        var t = new Date(lastmod);
        return t.toLocaleString();
    } else {
        return "unknown";
    }
}

function updateStyle(element, selected, isSelected) {
    if (isSelected) {
        element.addClass(selected);
    } else {
        element.removeClass(selected);
    }
}

function updateExample() {
    updateStyle($("#exprotocol"), "selected", $("#protocolCB").prop("checked"));
    updateStyle($("#exsubdomain"), "selected", $("#subdomainCB").prop("checked"));
    updateStyle($("#exdomain"), "selected", $("#domainCB").prop("checked"));
    updateStyle($("#expath"), "selected", $("#pathCB").prop("checked"));
}

function updateLeet() {
    $("#leetLevelLB").prop("disabled", $("#whereLeetLB").val() === "off");
    updateStyle($("#leetLevelLabel"), "disabled", $("#whereLeetLB").val() === "off");
}

function addProfile() {
    var p = Object.create(Profile);
    p.title = "No name";
    p.timestamp = Date.now();
    Settings.addProfile(p);
    updateProfileList();
    setCurrentProfile(p);
}

function removeProfile() {
    if (confirm("Really delete this profile?")) {
        Settings.deleteProfile(Settings.currentProfile);
        updateProfileList();
        setCurrentProfile(Settings.profiles[0]);
    }
}

function removeAllProfiles() {
    if (confirm("Really delete ALL local profile customizations and reset to the default profiles?")) {
        browser.storage.local.remove("profiles");
        Settings.loadLocalProfiles(() => { updateProfileList(); });
    }
}

function setCurrentProfile(profile) {
    Settings.currentProfile = profile.id;
    $("#profileNameTB").val(profile.title);
    $("#siteList").val((profile.siteList).replace(/\s/g, "\n"));
    $("#protocolCB").prop("checked", profile.url_protocol);
    $("#subdomainCB").prop("checked", profile.url_subdomain);
    $("#domainCB").prop("checked", profile.url_domain);
    $("#pathCB").prop("checked", profile.url_path);
    $("#inputUseThisText").val(profile.strUseText);
    $("#whereLeetLB").val(profile.whereToUseL33t);
    $("#leetLevelLB").val(profile.l33tLevel);
    $("#hashAlgorithmLB").val(profile.hashAlgorithm);
    $("#passwdLength").val(profile.passwordLength);
    $("#usernameTB").val(profile.username);
    $("#modifier").val(profile.modifier);
    $("#passwordPrefix").val(profile.passwordPrefix);
    $("#passwordSuffix").val(profile.passwordSuffix);
    $("#description").val(profile.description);
    document.getElementById("timestamp").textContent = formatLastModTime(profile.timestamp);

    $("#charset").empty();
    for (var i = 0; i < CHARSET_OPTIONS.length; i++) {
        $("#charset").append(new Option(CHARSET_OPTIONS[i]));
    }
    $("#charset").append(new Option("Custom charset"));

    if (CHARSET_OPTIONS.indexOf(profile.selectedCharset) >= 0) {
        $("#charset").val(profile.selectedCharset);
    } else {
        $("#charset").val("Custom charset");
        $("#customCharset").val(profile.selectedCharset);
    }

    updateCustomCharsetField();
    updateExample();
    updateLeet();
    highlightProfile();
    // Keeps profile #1 around so it can only be re-named
    if (Settings.profiles[0].id === profile.id) {
        $("#remove").hide();
        $("#makeDefaultButton").hide();
    } else {
        $("#remove").show();
        $("#makeDefaultButton").show();
    }
    if (Settings.alpha_sort_profiles != 0) {
        $("#moveUpButton").hide();
        $("#moveDownButton").hide();
    } else {
        idx = Settings.getProfileIndex(profile.id);
        if (idx == 0) {
            $("#moveUpButton").hide();
        } else {
            $("#moveUpButton").show();
        }
        if (idx < Settings.profiles.length - 1) {
            $("#moveDownButton").show();
        } else {
            $("#moveDownButton").hide();
        }
    }

    showSection("#profile_settings");
    setTimeout(function () {
        hashWarning(profile.hashAlgorithm);
    }, 0);
}

function updateCustomCharsetField() {
    if ($("#charset").val() === "Custom charset") {
        $("#customCharset").val(Settings.getProfile(Settings.currentProfile).selectedCharset).show();
    } else {
        $("#customCharset").hide();
    }
}

function hashWarning(hash) {
    // Be as annoying as possible to try and stop people from using the bugged algorithms
    var bugged = { "md5_v6": 1, "hmac-md5_v6": 1, "hmac-sha256": 1 };
    if (bugged[hash]) {
        if (confirm("Are you sure you want to continue using a legacy algorithm which is incorrectly implemented?")) {
            alert("Please change to using a correct & secure algorithm!\n\nThe old/bugged/legacy algorithms " +
                "are harmful to your online security and should be avoided at ALL costs.\n\n" +
                "Please change your passwords on the sites which you are using this algorithm if you are able to " +
                "as soon as possible.\n\nThank you\n");
        } else {
            alert("Please select one of the correct and secure hash algorithms below :)");
        }
    }
}

function showImport() {
    showSection("#import_settings");
}

function showExport() {
    showSection("#export_settings");
    $("#exportText").val(RdfImporter.dumpDoc());
}

function importRdf() {
    var txt = $("#importText").val();

    if (txt.trim().length === 0) {
        alert("Import text is empty");
        return false;
    }

    var rdfDoc = RdfImporter.loadDoc(txt);
    // Check that profiles have been parsed and are available before wiping current data
    if (rdfDoc.profiles.length > 0 && $("#importOverwrite").prop("checked")) {
        Settings.profiles = [];
    }

    if (RdfImporter.saveProfiles(rdfDoc.profiles) === 0) {
        alert("Sorry, no profiles found");
        return false;
    }

    updateProfileList();
}

function copyRdfExport() {
//    browser.permissions.request({ permissions: ["clipboardWrite"] }).then(granted => {
//        if (granted) {
            $("#exportText").get(0).select();
            document.execCommand("copy");
//        } else {
//            alert("Sorry, the permission was not granted.");
//        }
//    }).catch(reason => {
//        console.log("catch", reason);
//    });
}

function showOptions() {
    var setPromise = browser.storage.sync.get("synced_profiles");
    setPromise.then(
        //something in sync data
        function (data) {
            if (Object.keys(data).length > 0) {
                Settings.syncDataAvailable = true;
                Settings.syncPasswordOk = Boolean(Settings.decrypt(Settings.sync_profiles_password, data.synced_profiles));
            } else {
                Settings.syncDataAvailable = false;
                Settings.syncPasswordOk = false;
            }
        },
        //error accessing sync data
        function () {
            Settings.syncDataAvailable = false;
            Settings.syncPasswordOk = false;
        });

    $("#store_location").val(Settings.store_location);
    $("#expirePasswordMinutes").val(Settings.expire_password_minutes || 5);
    updateExpireTime();
    updateStyle($("#master_password_row"), "hidden", !Settings.keepMasterPasswordHash());
    updateSyncProfiles();
    showSection("#general_settings");
}

function showInformation() {
    showSection("#general_information");
}

function showSection(showId) {
    $("#checkStrength").prop("checked", false);
    showStrengthSection();
    $("section").add("aside").not(showId).css("display", "none");
    $(showId).css("display", "block");
}

function highlightProfile() {
    $(".highlight").removeClass("highlight");
    $("#profile_" + Settings.currentProfile).addClass("highlight");
}

function updateStorageLocation() {
    Settings.setStoreLocation($("#store_location").val());
    updateExpireTime();
}

function saveProfile() {
    var selected = Settings.getProfile(Settings.currentProfile);

    selected.title = $("#profileNameTB").val().trim();
    selected.siteList = $("#siteList").val().trim().split(/\s+/).join(" ");
    selected.url_protocol = $("#protocolCB").prop("checked");
    selected.url_subdomain = $("#subdomainCB").prop("checked");
    selected.url_domain = $("#domainCB").prop("checked");
    selected.url_path = $("#pathCB").prop("checked");
    selected.strUseText = $("#inputUseThisText").val().trim();
    selected.whereToUseL33t = $("#whereLeetLB").val();
    selected.l33tLevel = $("#leetLevelLB").val();
    selected.hashAlgorithm = $("#hashAlgorithmLB").val();
    selected.passwordLength = $("#passwdLength").val();
    selected.username = $("#usernameTB").val().trim();
    selected.modifier = $("#modifier").val().trim();
    selected.passwordPrefix = $("#passwordPrefix").val();
    selected.passwordSuffix = $("#passwordSuffix").val();
    selected.description = $("#description").val();
    selected.timestamp = Date.now();
    document.getElementById("timestamp").textContent = formatLastModTime(selected.timestamp);

    // make sure default profile siteList and strUseText stays blank/generic
    if (Settings.profiles[0].id === selected.id) {
        selected.siteList = "";
        selected.strUseText = "";
    }

    if ($("#charset").val() === "Custom charset") {
        selected.selectedCharset = $("#customCharset").val();
    } else {
        selected.selectedCharset = $("#charset").val();
    }

    Settings.saveProfiles();
    updateProfileList();
    highlightProfile();
    setTimeout(function () {
        hashWarning(selected.hashAlgorithm);
    }, 0);
}

function cloneProfile() {
    var p = Object.assign(Object.create(Profile), Settings.getProfile(Settings.currentProfile));
    p.title = p.title + " Copy";
    p.timestamp = Date.now();
    Settings.addProfile(p);
    updateProfileList();
    setCurrentProfile(p);
}

function makeDefault() {
    Settings.setDefaultProfile(Settings.currentProfile);
    updateProfileList();
}

function moveUp() {
    p = Settings.moveUp(Settings.currentProfile);
    updateProfileList();
    if (p) {
        setCurrentProfile(p);
    }
}

function moveDown() {
    p = Settings.moveDown(Settings.currentProfile);
    updateProfileList();
    if (p) {
        setCurrentProfile(p);
    }
}

function editProfile(event) {
    setCurrentProfile(Settings.getProfile(event.target.id.slice(8)));
}

function updateProfileList() {
    Settings.loadProfiles();
    Settings.alphaSortProfiles(Settings.shouldAlphaSortProfiles());

	document.getElementById("profile_num").textContent = Settings.profiles.length.toString();
    $("#profile_list").empty();
    var lastmod = 0;
    for (var i = 0; i < Settings.profiles.length; i++) {
        $("#profile_list").append(`<li><span id='profile_${Settings.profiles[i].id}' class='link'>${Settings.profiles[i].title}</span></li>`);
        if (Settings.profiles[i].timestamp > lastmod) {
            lastmod = Settings.profiles[i].timestamp;
        }
    }
    document.getElementById("profilelist_lastmod").textContent = formatLastModTime(lastmod);
}

function setSyncPassword() {
    if ($("#syncProfilesPassword").val() === "") {
        alert("Please enter a password to enable sync");
        return;
    }

    Settings.startSyncWith($("#syncProfilesPassword").val(), function () {
        $("#syncProfilesPassword").val("");
        updateSyncProfiles();
        updateProfileList();
    }, function () {
        alert("Wrong password. Please specify the password you used when initially syncing your data");
    });
}

function clearSyncData() {
    var clearPromise = browser.storage.sync.clear()

    clearPromise.then(
        //success clearing sync
        function () {
            Settings.sync_profiles = false;
            browser.storage.local.remove("synced_profiles");
            browser.storage.local.remove("synced_profiles_keys");
            browser.storage.local.remove("sync_profiles_password");
            Settings.sync_profiles_password = "";
            Settings.syncDataAvailable = false;
            Settings.syncPasswordOk = false;
            Settings.loadLocalProfiles(function () {
                Settings.syncDataAvailable = false;
                updateSyncProfiles();
                updateProfileList();
            });
        },
        //error clearing sync
        function () {
            alert("Could not delete synced data: " + browser.runtime.lastError.message);
        }
    );
}

function updateSyncProfiles() {
    $("#sync_profiles_row, #no_sync_password, #sync_data_exists, #sync_password_set").hide();
    $("#set_sync_password, #clear_sync_data").addClass("hidden");
    //console.log(`updateSyncProfiles syncPasswordOk ${Settings.syncPasswordOk}, synDataAvailable ${Settings.syncDataAvailable}`);
    if ($("#syncProfiles").prop("checked")) {
        if (Settings.syncPasswordOk) {
            $("#sync_password_set").show();
            $("#clear_sync_data").removeClass("hidden");
        } else if (Settings.syncDataAvailable) {
            $("#sync_profiles_row, #sync_data_exists").show();
            $("#set_sync_password, #clear_sync_data").removeClass("hidden");
        } else {
            $("#sync_profiles_row, #no_sync_password").show();
            $("#set_sync_password").removeClass("hidden");
        }
    } else {
        Settings.stopSync();
        updateProfileList();
    }
}

function updateMasterHash() {
    if ($("#keepMasterPasswordHash").prop("checked")) {
        $("#master_password_row").removeClass("hidden");
        var master_pass = $("#masterPassword").val();
        if (master_pass.length > 0) {
            browser.storage.local.set({ keep_master_password_hash: true, master_password_hash: JSON.stringify(Settings.make_pbkdf2(master_pass)) });
            Settings.keep_master_password_hash = true;
            Settings.master_password_hash = JSON.stringify(Settings.make_pbkdf2(master_pass))
        } else {
            browser.storage.local.set({ keep_master_password_hash: false });
            browser.storage.local.remove("master_password_hash")
            Settings.keep_master_password_hash = false;
            Settings.master_password_hash = "";
        }
    } else {
        $("#master_password_row").addClass("hidden");
        $("#masterPassword").val("");
        Settings.keep_master_password_hash = false;
        browser.storage.local.set({ keep_master_password_hash: false });
        Settings.master_password_hash = "";
        browser.storage.local.remove("master_password_hash")
    }
}

function updateHidePassword() {
    Settings.show_generated_password = $("#hidePassword").prop("checked");
    browser.storage.local.set({ show_generated_password: $("#hidePassword").prop("checked") });
}

function updateUseVerificationCode() {
    browser.storage.local.set({ use_verification_code: $("#useVerificationCode").prop("checked") });
    Settings.use_verification_code = $("#useVerificationCode").prop("checked");
}

function updatefillUsername() {
    browser.storage.local.set({ fill_username: $("#fillUsername").prop("checked") });
    Settings.fill_username = $("#fillUsername").prop("checked");
}

function updateRememberSiteProfile() {
    Settings.remember_site_profile = $("#rememberSiteProfile").prop("checked");
}

function updateShowStrength() {
    show = $("#showPasswordStrength").prop("checked");
    browser.storage.local.set({ show_password_strength: show });
    Settings.show_password_strength = show;
}

function updateAlphaSortProfiles() {
    do_sort = $("#alphaSortProfiles").val();
    browser.storage.local.set({ alpha_sort_profiles: do_sort });
    Settings.alpha_sort_profiles = do_sort;
    updateProfileList();
    filterProfiles()
}

function sanitizePasswordLength() {
    var field = $("#passwdLength");
    if (field.val() < 4) field.val("4");
    if (field.val() > 512) field.val("512");
}


function sanitizeExpireTime(newExpireTime) {
    var field = $("#expirePasswordMinutes");
    if (newExpireTime < 1) {
        newExpireTime = 1;
        field.val("1");
    }
    if (newExpireTime > 720) {
        newExpireTime = 720;
        field.val("720");
    }
    newExpireTime = parseInt(newExpireTime, 10);
    field.val(newExpireTime);
    return newExpireTime;
}

function updateExpireTime() {
    var shouldExpire = Settings.store_location === "memory_expire";
    var oldExpireTime = Settings.expire_password_minutes || 5;
    var newExpireTime = $("#expirePasswordMinutes").val();
    if (shouldExpire) {
        newExpireTime = sanitizeExpireTime(newExpireTime);
        if (newExpireTime !== oldExpireTime) {
            browser.storage.local.set({ expire_password_minutes: newExpireTime });
            Settings.expire_password_minutes = newExpireTime;
            Settings.createExpirePasswordAlarm(newExpireTime);
        }
    } else {
        browser.alarms.clear("expire_password");
    }
    updateStyle($("#password_expire_row"), "hidden", !shouldExpire);
}

function fileImport() {
    var file = $("#fileInput")[0].files[0];
    if ((/rdf|xml/i).test(file.type)) {
        var reader = new FileReader();
        reader.onload = () => {
            $("#importText").val(reader.result);
        };
        reader.readAsText(file);
//        reader.readAsBinaryString(file);
    } else {
        $("#importText").val("Please select an RDF or XML file containing PasswordMaker profile data.");
    }
}

function fileExport() {
    var textFileAsBlob = new Blob([$("#exportText").val()], {
        type: "application/rdf+xml"
    });
    var downloadLink = document.createElement("a");
    downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
    downloadLink.download = "PasswordMaker Profile Data.rdf";
    downloadLink.click();
}

function showStrengthSection() {
    if ($("#checkStrength").prop("checked")) {
        $("main").width("1150px");
        $("#strength_section").css("display", "inline-block");
        $(".testInput").on("change keyup", checkPassStrength);
        checkPassStrength();
    } else {
        $("main").width("900px");
        $("#strength_section").hide();
        $(".testInput").off("change keyup", checkPassStrength);
        $(".strengthInput").val("");
    }
}

function filterProfiles() {
    var filter = document.getElementById("searchProfiles").value.toUpperCase();
    var list = document.getElementById("profile_list").getElementsByTagName("li");

    // Loop through all list items, and hide those which don't match the search query
    for (var i = 0; i < list.length; i++) {
        var itemid = list[i].getElementsByTagName("span")[0].id.substr(8);
//        if (items.innerHTML.toUpperCase().indexOf(filter) > -1) {
    	var prof = Settings.profiles[parseInt(itemid)-1];
		if ( prof.title.toUpperCase().indexOf(filter) > -1 || prof.strUseText.toUpperCase().indexOf(filter) > -1 || prof.username.toUpperCase().indexOf(filter) > -1 || prof.description.toUpperCase().indexOf(filter) > -1 || prof.siteList.toUpperCase().indexOf(filter) > -1 ) {
            list[i].style.display = "";
        } else {
            list[i].style.display = "none";
        }
    }
}

function checkPassStrength() {
    var selected = Settings.getProfile(Settings.currentProfile);

    selected.siteList = $("#siteList").val().trim().replace(/[*?$+()^\[\]\\|{},]/g, "").split(/\s+/).shift();
    selected.url_protocol = $("#protocolCB").prop("checked");
    selected.url_subdomain = $("#subdomainCB").prop("checked");
    selected.url_domain = $("#domainCB").prop("checked");
    selected.url_path = $("#pathCB").prop("checked");
    selected.strUseText = $("#inputUseThisText").val().trim();
    selected.whereToUseL33t = $("#whereLeetLB").val();
    selected.l33tLevel = $("#leetLevelLB").val();
    selected.hashAlgorithm = $("#hashAlgorithmLB").val();
    selected.passwordLength = $("#passwdLength").val();
    selected.username = $("#usernameTB").val().trim();
    selected.modifier = $("#modifier").val().trim();
    selected.passwordPrefix = $("#passwordPrefix").val();
    selected.passwordSuffix = $("#passwordSuffix").val();
    selected.description = $("#description").val();

    if ($("#charset").val() === "Custom charset") {
        selected.selectedCharset = $("#customCharset").val();
    } else {
        selected.selectedCharset = $("#charset").val();
    }

    if (selected.getText().length !== 0) {
        $("#testText").val(selected.getText());
    } else {
        $("#testText").val(selected.getUrl(selected.siteList));
    }

    $("#genPass").val(selected.getPassword($("#testText").val(), $("#testPass").val(), selected.username));
    var values = Settings.getPasswordStrength($("#genPass").val());
    $("#genStrength, meter").val(values.strength);
    $("#hasUpper").prop("checked", values.hasUpper);
    $("#hasLower").prop("checked", values.hasLower);
    $("#hasDigit").prop("checked", values.hasDigit);
    $("#hasSymbol").prop("checked", values.hasSymbol);
}

function thisbottomHalf() {
    updateProfileList();
    //console.log(`Listener Profiles: ${Settings.profiles}`);
    setCurrentProfile(Settings.profiles[0]);

    $("#hidePassword").prop("checked", Settings.shouldHidePassword());
    $("#keepMasterPasswordHash").prop("checked", Settings.keepMasterPasswordHash());
    $("#useVerificationCode").prop("checked", Settings.useVerificationCode());
    $("#fillUsername").prop("checked", Settings.shouldFillUsername());
    $("#rememberSiteProfile").prop("checked", Settings.remember_site_profile);
    $("#showPasswordStrength").prop("checked", Settings.shouldShowStrength());
    $("#syncProfiles").prop("checked", Settings.shouldSyncProfiles());
    $("#alphaSortProfiles").val(Settings.shouldAlphaSortProfiles());

    $("#profile_list").on("click", ".link", editProfile);
    $("#add").on("click", addProfile);
    $("#showImport").on("click", showImport);
    $("#showExport").on("click", showExport);
    $("#showSettings").on("click", showOptions);
    $("#showInformation").on("click", showInformation);

    $("#protocolCB").on("change", updateExample);
    $("#subdomainCB").on("click", updateExample);
    $("#domainCB").on("click", updateExample);
    $("#pathCB").on("click", updateExample);
    $("#whereLeetLB").on("change", updateLeet);
    $("#charset").on("change", updateCustomCharsetField);
    $("#passwdLength").on("blur", sanitizePasswordLength);

    $("#cloneProfileButton").on("click", cloneProfile);
    $("#makeDefaultButton").on("click", makeDefault);
    $("#moveUpButton").on("click", moveUp);
    $("#moveDownButton").on("click", moveDown);
    $("#checkStrength").on("change", showStrengthSection);
    $("#remove").on("click", removeProfile);
    $("#save").on("click", saveProfile);
    $("#importButton").on("click", importRdf);
    $("#fileInput").on("change", fileImport);
    $("#copyButton").on("click", copyRdfExport);
    $("#exportFileButton").on("click", fileExport);

    $("#store_location").on("change", updateStorageLocation);
    $("#expirePasswordMinutes").on("change", updateExpireTime);
    $("#hidePassword").on("change", updateHidePassword);
    $("#keepMasterPasswordHash").on("change", updateMasterHash);
    $("#syncProfiles").on("change", updateSyncProfiles);
    $("#masterPassword").on("keyup", updateMasterHash);
    $("#useVerificationCode").on("change", updateUseVerificationCode);
    $("#fillUsername").on("change", updatefillUsername);
    $("#rememberSiteProfile").on("change", updateRememberSiteProfile);
    $("#showPasswordStrength").on("change", updateShowStrength);
    $("#alphaSortProfiles").on("change", updateAlphaSortProfiles);
    $("#set_sync_password").on("click", setSyncPassword);
    $("#clear_sync_data").on("click", clearSyncData);
    $("#resetToDefaultprofiles").on("click", removeAllProfiles);
    $("#searchProfiles").on("input", filterProfiles);
}

document.addEventListener("DOMContentLoaded", () => {
    //console.log(`options: Profile number ${Settings.profiles.length}`);
    ObtainSettings.then(() => { Settings.loadProfiles(thisbottomHalf) });
});
