# PasswordMaker Quantum

A new fork with improvements based on the firefox port of chrome-passwordmaker-pro by Peter Maunz.
Install it from the firefox addon store: https://addons.mozilla.org/en-US/firefox/addon/passwordmaker-quantum/

## Maintenance on May, 2023:
- remove unnecessary clipboardRead and downloads permissions
- code cleanups and minor changes
- will continue to do limited updates in sync with https://github.com/spartanroc/chrome-passwordmaker

## Updates on Dec, 2018:
- search profiles works now and query for fields of title/name,siteList,strUseText,username, and description
- import rdf with correct encodings
- implement the description field that was used in the original PasswordMaker. works with import/export.
- better siteList examples
- scroll bar for profile list (options.css)
- copy-passowrd button bug got fixed (popup.js and clipboardWrite permission required in manifest.json)
- based on the firefox port of chrome-passwordmaker-pro by Peter Maunz
- Peter Maunz's firefox port: https://gitlab.com/pmaunz/chrome-passwordmaker
- original chrome extension: https://github.com/passwordmaker/chrome-passwordmaker


![Example Screenshot](http://i.imgur.com/OxCD9TN.png)

The only thing which kept me from completely switching to Chrome was a missing port of the [passwordmaker.org](http://www.passwordmaker.org/) extension.
There was one available in the [Chrome Web Store](https://chrome.google.com/webstore/detail/passwordmaker/doblembglfahhpiilfhajboogopikhcm) but sadly, that extension is only a simple port of the HTML page which is offered on [passwordmaker.org](http://www.passwordmaker.org/passwordmaker.html).

This extension fixes that.  This is a native Chrome port with all the bells and whistles you'd expect!

# Installation

###Chrome
  - You can install this extension from the [Chrome Web Store](https://chrome.google.com/webstore/detail/passwordmaker-pro/lnhofcfhehhcbccpmdmdpjncdoihmkkh) from it's new home.

###Opera
  - You can install this extension from the [Opera Extension Gallery](https://addons.opera.com/en/extensions/details/passwordmaker-pro/)

###Firefox
  - You can install this extension from the [Mozilla addon repository](https://addons.mozilla.org/en-US/firefox/addon/firefox-passwordmaker-pro/)
    It is tested on the stable release 48.0 and the nightly build 58.0

If you have previously used the Firefox Extension, you can directly import your .rdf file.

# Status of this project

## 2017-10-06 The port of the Chrome extension to Firefox is maintained by [yb171](https://github.com/yb171/chrome-passwordmaker).

## Update 2014-07-13 by [@heavensrevenge](https://github.com/heavensrevenge)

I am the new and active maintainer of this project and I have uploaded this extension back to the Chrome Web Store which is located at https://chrome.google.com/webstore/detail/passwordmaker-pro/lnhofcfhehhcbccpmdmdpjncdoihmkkh

**_PLEASE Export your profile data_** to be sure you have a back-up of your data which you can import into the most current version.
I apologize for any problems the disappearance from the Chrome Web Store may have caused, but I will do my best to keep this extension alive and well.


### Update 2014-07-12 by [@bitboxer](http://github.com/bitboxer)

I don't use this plug-in anymore and do not have the time or energy to continue maintainership.
I'm out. It was a nice ride.

# Note on Patches/Pull Requests:

* Fork the project.
* Make your feature addition or bug fix.
* Commit, do not mess with version in manifest.json
  (if you want to have your own version, that is fine but bump version in a commit by itself I can ignore when I pull)
* Send me a pull request. Bonus points for topic branches.

# Copyright

See LICENSE for details. A list of all contributors can be found [here](http://github.com/passwordmaker/chrome-passwordmaker/contributors).
